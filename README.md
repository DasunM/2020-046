1. Objectives
1.1 Main Objective

As a solution we are going to implement child intelligent assessment tool. It is an android application nonverbal tests can be done easily through this application.

Among them the drawing done by a child in draw a person test will be recognized and how child is drawing the figure (figure movement recognition) will also be detected by the application. Then according to the drawing the body parts of the person draw will be identified. With use of a mathematical function marks allocation and measuring growing age will be done. Later detailed report with all the information and facility to backup them will be provided.


1.2 Specific Objectives

Object 1: Detect the image and identify the body part.

Child intelligent assessment tool will save the drawing done by child. If he is using paper and pencil the image will be obtained using camera function. If not the drawing can be done by the paint app given through the app. Identification of the body parts of the drawing which is done by the paint app or taken through one camera function.

Object 2: Give the score to the body parts and calculate the growing age.

The body parts in the drawing of the child will be identified and marks will be given. It is a standard scoring mechanism done using the learning assessment book. After taking the total of those values it will be compared with a standard table and growing age will be obtained.

Object 3: Generate detailed report and backup

There detailed given by the child, his current age and developed age will be shown also marks obtained through the test and all other information and all other information will be included.

Object 4: Track the figure Movement 

There how the child will make approach to draw the figure and how he will end it, which body part will be draw at last will be determined. Report about this also will be added to the main report through a tool.


2.0 Methodology

Here, the previous data required for the test is provided by the lady Ridgway hospital. Currently, we have requested six months of data and its sample size is 120 patient records. The main data taken here is their register no, age, test results. After collecting these data and preparing the data correctly and evaluate algorithms for it. Finally, improve the result and present it.

Main things producing by Draw-a-person in Test child intelligent assessment tool is,
Identify the image drawn by child "good enough Harris draw a woman test".
* Detect the body parts of the image and give the test results.
* Generate the function to compare and give the marks to drawn body parts.
* Implement a function to calculate the performance and give an IQ level.
* Implement an arithmetic test for diagnose IQ level.
* detect figure tracking.

Drawing the map test is to be done through the android application. The application gets the image drawn by the child in due time. At the same time, the application tracks the children’s figure tracking movements. Then the application detects the body parts that have been drawn from the image. Then customized mathematical function assigns marks to the detected body parts. After that, the score compares with the standard table and given a growing age. It generates the results and stores it using the child’s register id. Then the application provides a detailed report about the results. If child doesn’t like to use mobile phone they can continue the test manually. After finishing the test the app provides a camera facility to collect the information. After getting these details it works like as mentioned before.